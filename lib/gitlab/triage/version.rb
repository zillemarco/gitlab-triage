# frozen_string_literal: true

module Gitlab
  module Triage
    VERSION = '1.26.0'
  end
end
